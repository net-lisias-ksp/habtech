# HabTech

WiP

## References

* [benjee10](https://forum.kerbalspaceprogram.com/index.php?/profile/84110-benjee10/)
	+ [KSP Forum](https://forum.kerbalspaceprogram.com/index.php?/topic/133501-*)
	+ [SpaceDock](https://spacedock.info/mod/449/HabTech) CC-BY-NC-SA
	+ [SpaceDock](https://spacedock.info/mod/2078/HabTech2) ARR
	+ [GitHub](https://github.com/benjee10/HabTech2) ARR
* [LouisB3](https://github.com/LouisB3) 
	+ [GitHub](https://github.com/LouisB3/HabTech-Classic) Community maintenance
